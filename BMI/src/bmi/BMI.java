package bmi;

public class BMI 
{
	int koerperGroesseInCM;
	double koerperGewicht;
	boolean weiblich = false;
	
	public BMI(int kGr,double kGw,boolean fem)
	{
		this.koerperGroesseInCM = kGr;
		this.koerperGewicht = kGw;
		this.weiblich=fem;
	}
	
	public BMI() 
	{
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args)
	{
		BMI b = new BMI(183,112,false);
		b.getBMI();
		b.getGeschlecht();
	}
	
	public void getGeschlecht()
	{
		
		String geschlecht="male";
		
		if(weiblich)
		{
			geschlecht="femnale";
			System.out.println(geschlecht);
		}
		System.out.println(geschlecht);
	}
	
	public void getBMI()
	{
		if(!weiblich)
		{
			double bmi = 0.0;
			//double gr = (double)koerperGroesseInCM/100;
			bmi =(koerperGewicht/Math.pow(((double)koerperGroesseInCM/100),2));
			System.out.printf("%.2f\n",bmi);
		}
		
	}


	public int getKoerperGroesseInCM() {
		return koerperGroesseInCM;
	}


	public void setKoerperGroesseInCM(int koerperGroesseInCM) {
		this.koerperGroesseInCM = koerperGroesseInCM;
	}


	public double getKoerperGewicht() {
		return koerperGewicht;
	}


	public void setKoerperGewicht(double koerperGewicht) {
		this.koerperGewicht = koerperGewicht;
	}


	public boolean isWeiblich() {
		return weiblich;
	}


	public void setWeiblich(boolean weiblich) {
		this.weiblich = weiblich;
	}
	
}
